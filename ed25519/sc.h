/* Copyright (C) 2014, Hsiang Kao <esxgx@163.com>
 *
 * This part is based on nightcracker's ed25519.
 * <Ref2 https://github.com/nightcracker/ed25519/>
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */
#ifndef SC_H
#define SC_H

/*
The set of scalars is \Z/l
where l = 2^252 + 27742317777372353535851937790883648493.
*/

void sc_reduce(unsigned char *s);
void sc_muladd(unsigned char *s, const unsigned char *a, const unsigned char *b, const unsigned char *c);

#endif